from utilitaires import verification_string, lettre_par_indice, indice_par_lettre


def demander_decalage():
    """
    Cette fonction demande à l'utilisateur le décalage de lettres qu'il souhaite pour son message.

    Elle ne nécessite aucun paramètre.
    Elle retourne un entier (le décalage de lettres choisi).
    """
    print("Le code César remplace toutes les lettres d'un message par des lettres à n rangs plus loin.")
    decalage = input("De combien de lettres souhaitez-vous que le décalage (donc n) soit ? ")

    while not decalage.isdigit():
        print("Vous devez nous renseigner un entier.")
        decalage = input("De combien de lettres souhaitez-vous que le décalage (donc n) soit ? ")

    return int(decalage)


def chiffrement(message, decalage):
    """
    Cette fonction retourne la chaîne de caractère donnée en paramètre chiffrée avec l'algorithme de chiffrement de César.

    En paramètre, elle nécessite une chaîne de caractère ne contenant que des lettres minuscules.
    Elle retourne une chaîne de caractère également (le message chiffré).
    """

    assert verification_string(message)

    retour = ""

    for lettre in message:
        if lettre == " ":
            retour += " " # On met un espace automatiquement pour les espaces dans le message.
            continue

        retour += lettre_par_indice((indice_par_lettre(lettre) + decalage) % 26)

    return retour


def chiffrer(message):
    """
    Cette fonction va demander à l'utilisateur le décalage de lettres du message et va retourner la chaîne de caractère donnée en paramètre chiffrée avec l'algorithme de chiffrement de César.

    En paramètre, elle nécessite une chaîne de caractère ne contenant que des lettres minuscules.
    Elle retourne une chaîne de caractère également (le message chiffré).
    """
    decalage = demander_decalage()
    return chiffrement(message, decalage)


def dechiffrer(message):
    """
    Cette fonction va demander à l'utilisateur le décalage de lettres du message et va retourner la chaîne de caractère donnée en paramètre déchiffrée avec l'algorithme de chiffrement de César.

    En paramètre, elle nécessite une chaîne de caractère ne contenant que des lettres minuscules.
    Elle retourne une chaîne de caractère également (le message déchiffré).
    """
    decalage = demander_decalage()
    return chiffrement(message, -decalage)
