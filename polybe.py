from utilitaires import verification_string

nbre_ligne = 5
nbre_colonne = 5

def demander_grille():
    """
    Cette fonction demande à l'utilisateur une chaîne de caractère qui va être par la suite transformée en grille de lettres.

    Elle ne nécessite aucun paramètre.
    Elle retourne une tableau à 2 dimensions (tableau de tableaux).
    """
    print("Le code de Polybe nécessite une grille de 25 lettres (de 5 lignes et 5 colonnes).")
    print("Vous allez être demandé d'écrire les 25 lettres (minuscules) de ligne en ligne à la suite.")
    print("Par exemple, si vous avez la grille suivante :")
    print("a b c d e")
    print("f g h i j")
    print("k l m n o")
    print("p q r s t")
    print("u v w x y")
    print("Vous allez devoir écrire : abcdefghijklmnopqrstuvwxy.")
    valeur = input("Veuillez rentrer la grille comme expliqué ci-dessus : ")

    while not verification_string(valeur, avec_espace=False) or len(valeur) != (nbre_ligne * nbre_colonne): # Si la valeur donnée n'est pas recevable par le programme.
        print("La grille doit contenir 25 lettres minuscules.")
        valeur = input("Veuillez rentrer la grille comme expliqué ci-dessus : ")

    grille = []

    for i in range(nbre_ligne):
        grille.append([""] * nbre_colonne) # Initialisation du tableau à deux dimensions

    cumul = 0 # Création d'un cumul

    for i in range(nbre_ligne):
        for j in range(nbre_colonne):
            grille[i][j] = valeur[cumul] # Ajout des lettres dans la grille
            cumul += 1

    return grille


def position_par_lettre(grille):
    """
    Cette fonction retourne la position de toutes les lettres dans une grille donnée.

    En paramètre, elle nécessite une grille de lettres.
    Elle retourne un dictionnaire ayant comme clef une lettre et en valeur un tuple contenant deux entiers (l'indice de la lettre dans la grille).
    """
    assert len(grille) == nbre_ligne 
    assert len(grille[0]) == nbre_colonne # Assertions de la taille de la grille

    positions = {} # Initialisation du dictionnaire

    for i in range(nbre_ligne):
        for j in range(nbre_colonne):
            lettre = grille[i][j]
            positions[lettre] = (i, j) # Ajout des indices par rapport à la lettre dans le dictionnaire

    return positions


def chiffrer(message):
    """
    Cette fonction retourne la chaîne de caractère donnée en paramètre chiffrée avec l'algorithme de chiffrement de Polybe.

    En paramètre, elle nécessite une chaîne de caractère ne contenant que des lettres minuscules.
    Elle retourne une chaîne de caractère également (le message chiffré).
    """

    assert verification_string(message)

    grille = demander_grille()
    positions = position_par_lettre(grille)

    retour = "" # Initialisation message

    for lettre in message:
        if lettre in positions:
            position = positions[lettre]
            retour += str(position[0] + 1) + str(position[1] + 1) # Récupération des indices de la lettre et ajout dans le message chiffré

    return retour


def dechiffrer(message):
    """
    Cette fonction retourne la chaîne de caractère donnée en paramètre déchiffrée avec l'algorithme de chiffrement de Polybe.

    En paramètre, elle nécessite une chaîne de caractère ne contenant que des lettres minuscules.
    Elle retourne une chaîne de caractère également (le message déchiffré).
    """

    while not message.isdigit() or len(message) % 2 != 0: # Si la valeur donnée n'est pas recevable par le programme.
        print("Lors du déchiffrement avec le code de Polybe, le message ne doit être composé de chiffres, et la taille du message doit être un multiple de 2.")
        message = input("Le message : ")

    grille = demander_grille()
    message = [message[i:i + 2] for i in range(0, len(message), 2)] # Coupage de la chaîne tous les 2 caractères en un tableau de chaîne de caractères 

    retour = "" # Initialisation message

    for lettre in message:
        ligne = int(lettre[0]) - 1 # Récupération ligne par l'indice
        colonne = int(lettre[1]) - 1 # Récupération colonne par l'indice

        retour += grille[ligne][colonne] # Récupération lettre par ligne et colonne

    return retour
