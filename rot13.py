from utilitaires import verification_string, lettre_par_indice, indice_par_lettre


def chiffrer(message):
    """
    Cette fonction retourne la chaîne de caractère donnée en paramètre chiffrée avec l'algorithme de chiffrement ROT13.

    En paramètre, elle nécessite une chaîne de caractère ne contenant que des lettres minuscules.
    Elle retourne une chaîne de caractère également.
    """

    assert verification_string(message)

    retour = "" # Initialisation message
    for lettre in message:
        if lettre == " ":
            retour += " "
            continue
        # Pour chaque lettre de message est ajoutée une lettre 13 rangs plus loin dans l'alphabet.
        # Nous récupérons la lettre à l'indice de lettre + 13, le tout modulo 26.

        # Nous utilisons modulo ici car si l'indice de lettre + 13 est au-dessus de la taille de l'alphabet (26),
        # modulo nous donnera le reste de la division (indice de lettre + 13) / 26, qui équivaut ici à revenir
        # à récupérer l'indice des lettres du début de l'alphabet.

        # Exemple : (indice de o (14) + 13) % 26 vaut 1, l'indice de la lettre b.
        retour += lettre_par_indice(indice_par_lettre(lettre) + 13)
    return retour


def dechiffrer(message):
    """
    Cette fonction retourne la chaîne de caractère donnée en paramètre déchiffrée avec l'algorithme de chiffrement
    ROT13.

    En paramètre, elle nécessite une chaîne de caractère ne contenant que des lettres minuscules.
    Elle retourne une chaîne de caractère également.
    """

    assert verification_string(message)

    # Étant donné que le ROT13 remplace les lettres par celles 13 rangs plus loin,
    # Et qu'il y a 26 lettres dans l'alphabet (2 fois plus), il est juste nécessaire de "rechiffrer"
    # le message déjà chiffré pour le déchiffrer.
    return chiffrer(message)
