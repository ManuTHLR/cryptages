from utilitaires import indice_par_lettre, lettre_par_indice, verification_string


def demander_clef():
    """
    Cette fonction demande à l'utilisateur la clef qu'il souhaite pour son message.

    Elle ne nécessite aucun paramètre.
    Elle retourne un entier (la clef choisie choisi).
    """
    print("Le chiffrement par code de Vigenère nécessite une clef.")
    clef = input("Veuillez écrire la clef : ")

    while not verification_string(clef): # Si la valeur donnée n'est pas recevable par le programme.
        clef = input("Veuillez écrire la clef : ")

    return clef


def chiffrer(message):
    """
    Cette fonction va demander à l'utilisateur la clef du message et va retourner la chaîne de caractère donnée en paramètre chiffrée avec l'algorithme de chiffrement de Vigenère.

    En paramètre, elle nécessite une chaîne de caractère ne contenant que des lettres minuscules.
    Elle retourne une chaîne de caractère également (le message chiffré).
    """

    assert verification_string(message)
    
    clef = demander_clef()

    retour = "" # Initialisation message
    for i in range(len(message)): # Boucle pour tous les entiers entre 0 et le nombre de caractères dans le message.
        lettre = message[i]

        if lettre == " ":
            retour += " "
            i -= 1 # On ne prend pas en compte les espaces dans l'indice des lettres du message.
            continue

        indice_clef = indice_par_lettre(clef[i % len(clef)]) # Récupérer l'indice dans la clef
        retour += lettre_par_indice(indice_par_lettre(lettre) + indice_clef) # L'additionner à l'indice de la lettre dans le message, puis récupérer la lettre à cet indice
    return retour


def dechiffrer(message):
    """
    Cette fonction va demander à l'utilisateur la clef du message et va retourner la chaîne de caractère donnée en paramètre déchiffrée avec l'algorithme de chiffrement de Vigenère.

    En paramètre, elle nécessite une chaîne de caractère ne contenant que des lettres minuscules.
    Elle retourne une chaîne de caractère également (le message déchiffré).
    """

    assert verification_string(message)

    clef = demander_clef()

    retour = "" # Initialisation message
    for i in range(len(message)):
        lettre = message[i]

        if lettre == " ":
            retour += " "
            i -= 1 # On ne prend pas en compte les espaces dans l'indice des lettres du message.
            continue

        indice_clef = indice_par_lettre(clef[i % len(clef)]) # Récupérer l'indice dans la clef
        retour += lettre_par_indice((indice_par_lettre(lettre) - indice_clef)) # Le soustraire à l'indice de la lettre dans le message, puis récupérer la lettre à cet indice
    return retour
