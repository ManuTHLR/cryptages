import cesar
import polybe
import rot13
import vigenere
from utilitaires import verification_string

# C'est un dictionnaire répertoriant les fonctions de chiffrement et déchiffrement et si le message donné doit subir une
# vérification au chiffrement, et au déchiffrement de chaque algorithme de chiffrement par leur nom.
algorithmes = {
    # Contenu
    # Nom: (fonction pour chiffrer, fonction pour déchiffrer, vérification du message à chiffrer ?,
    # vérification du message à déchiffrer ?)
    "rot13": (rot13.chiffrer, rot13.dechiffrer, True, True),
    "cesar": (cesar.chiffrer, cesar.dechiffrer, True, True),
    "vigenere": (vigenere.chiffrer, vigenere.dechiffrer, True, True),
    "polybe": (polybe.chiffrer, polybe.dechiffrer, True, False)
}

# Message de bienvenue
print("Bienvenue sur mon outil de chiffrement de message.")
print("Attention : Vos messages ne doivent contenir que des lettres de l'alphabet latin minuscules ou des espaces.")
print("Voici les algorithmes disponibles :")

for algorithme in algorithmes.items():
    # La fonction items des dictionnaires retourne des tuples contenant chaque clef ainsi que sa valeur.
    nom = algorithme[0]  # Le nom, étant la clef dans le dictionnaire, est à l'indice 0 de chaque tuple.
    print(f"- {nom}")  # Nous imprimons dans la console le nom de l'algorithme, en utilisant les f-strings de Python.

reponse = input("Quel algorithme souhaitez-vous utiliser ? : ")

while reponse not in algorithmes:
    print("L'algorithme suivant n'est pas proposé par ce programme.")
    print("Veuillez en choisir un diponible.")
    reponse = input("Quel algorithme souhaitez-vous utiliser cette fois-ci ? : ")

# Nous récupérons l'algorithme dans le dictionnaire via la réponse donnée par l'utilisateur.
# Nous allons ensuite décomposer l'algorithme par les deux valeurs dans le tuple retourné par le dictionnaire,
# la fonction de chiffrement et de déchiffrement de l'algorithme choisi.

algorithme = algorithmes[reponse]

print(f"Très bien ! Vous avez donc choisi l'algorithme {reponse}.")
print("Désormais, vous allez devoir nous dire si vous voulez chiffrer un message, ou le déchiffrer.")
print("- En tapant 1, vous allez pouvoir lancer la procédure de chiffrement avec l'algorithme " + reponse)
print("- En tapant 2, vous allez pouvoir lancer la procédure de déchiffrement avec ce même algorithme.")

reponse = input("1 ou 2 ? ")

while reponse != "1" and reponse != "2":
    print("Vous devez choisir entre 1 et 2.")
    reponse = input("1 ou 2 ? ")

# La réponse étant une chaîne de caractère, nous devons la convertir en entier.
reponse = int(reponse)
# Nous choisissons entre la fonction de chiffrement ou de déchiffrement selon la réponse de l'utilisateur.
fonction_choisie = algorithme[reponse - 1]

print("Désormais, vous allez devoir nous dire votre message : ")
message = input("Le message : ")

if algorithme[reponse + 1]:
    while not verification_string(message):
        print("Malheureusement, votre message secret ne doit contenir que les lettres minuscules et des espaces...")
        print("Vous allez devoir nous le dire une seconde fois !")
        reponse = input("Le message : ")

print(f"Voici le résultat : {fonction_choisie(message)}")