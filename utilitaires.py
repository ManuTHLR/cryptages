alphabet = "abcdefghijklmnopqrstuvwxyz"

def lettre_par_indice(indice):
    """
    Cette fonction retourne la lettre de l'alphabet à l'indice donné.

    En paramètre, elle nécessite un indice.
    Elle retourne un caractère (la lettre).
    """
    return alphabet[indice % len(alphabet)] # Utilisation du modulo car utilisé par la plupart des algorithmes pour récupérer une lettre même si l'indice > 26.

def indice_par_lettre(lettre):
    """
    Cette fonction retourne l'indice de l'alphabet de la lettre donnée.

    En paramètre, elle nécessite une lettre.
    Elle retourne un entier (l'indice).
    """
    return alphabet.index(lettre)

def verification_string(valeur, avec_espace=True):
    """
    Cette fonction vérifie que la chaîne de caractère ne contienne que des lettres minuscules (et des espaces si avec_espace vaut True).

    En paramètre, elle nécessite une chaîne de caractère et optionnellement un booléen avec_espace.
    Elle retourne un booléan (si la chaîne de caractère est acceptée ou non selon les conditions données).
    """
    for lettre in valeur:
        if lettre not in alphabet and not (avec_espace and lettre.isspace()): # Si une lettre n'est pas dans l'alphabet ou n'est pas un espace (si avec_espace == True)
            return False # On retourne False
    return True # Sinon True
